package com.example.app1_clase;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import BaseDatos.EscuelaBD;
import Entidades.Alumno;

@SuppressWarnings("unchecked")
public class ActivityConsultas extends Activity {
    ListView lista;
    RecyclerView recicler;
    RecyclerView.Adapter adaper;
    RecyclerView.LayoutManager layoutManager;
    List<Alumno> registros;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultas);
        lista = findViewById(R.id.listview1);


        /*new Thread(new Runnable() {
            @Override
            public void run() {
                EscuelaBD conexion=EscuelaBD.gettAppDatabase(getBaseContext());
                e= conexion.alumnoDAO().optenerTodos();

                for (Alumno a:e) {
                    Log.d("datos->", a.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayAdapter adaptador=new ArrayAdapter(getBaseContext(),
                                android.R.layout.simple_list_item_1,e);
                        lista.setAdapter(adaptador);

                    }
                });

            }
        }).start();*/

    }

    public void con(View v){
        recicler = findViewById(R.id.recyclerView1);

        recicler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);

        recicler.setLayoutManager(layoutManager);

        new Thread(new Runnable() {
            @Override
            public void run() {

                EscuelaBD conexion = EscuelaBD.gettAppDatabase(getBaseContext());
                registros = conexion.alumnoDAO().optenerTodos();

                String datos[] = new String[registros.size()];

                short cont = 0;

                for (Alumno d : registros) {
                    if (cont == 0) {
                        datos[cont++] = "------------------------------------\n" + d.toString() + "\n-------------------------------------";
                    } else {
                        datos[cont++] = d.toString() + "\n-------------------------------------";
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adaper = new AdaptadorRegistros(datos);
                        recicler.setAdapter(adaper);

                    }
                });

            }
        }).start();

    }
}

class AdaptadorRegistros extends RecyclerView.Adapter<AdaptadorRegistros.MyViewHolder>{

    private String[] mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public MyViewHolder(TextView t) {
            super(t);
            textView = t;
        }
    }

    public AdaptadorRegistros(String [] mDataset){
        this.mDataset = mDataset;
    }

    @NonNull
    @Override
    public AdaptadorRegistros.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.textviewreciclerview,
                parent, false);
        MyViewHolder vh = new MyViewHolder(tv);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.textView.setText(mDataset[position]);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}