package com.example.app1_clase;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import BaseDatos.EscuelaBD;
import Entidades.Alumno;

public class ActivityBajas extends AppCompatActivity {

    EditText txt_baja_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bajas);
        txt_baja_id = findViewById(R.id.txt_baja_id);
    }

    public void baja(View v){

        new Thread(new Runnable() {
            @Override
            public void run() {

                EscuelaBD conexionBD=EscuelaBD.gettAppDatabase(getBaseContext());
                conexionBD.alumnoDAO().eliminarPorNumControl(txt_baja_id.getText().toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getBaseContext(),"Eliminado con exito",Toast.LENGTH_LONG).show();

                    }
                });
            }

        }).start();

    }

}
